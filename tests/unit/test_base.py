# -*- coding: utf-8 -*-

"""

@version: ??
@author: xyj
@license: MIT License
@contact: xieyingjun@vip.qq.com
@Created on 2018/4/3 14:03
"""
from unittest import mock
import unittest

from xyj.base import Calculator


class MockDemo(unittest.TestCase):
    """测试demo"""

    def test_add(self):
        """
        测试加法方法

        :return:
        """
        count = Calculator()
        # 通过Mock类模拟被调用的方法add()方法，return_value 定义add()方法的返回值
        # count.add = mock.Mock(return_value=16)
        # side_effect参数它给mock分配了可替换的结果，覆盖了return_value
        count.add = mock.Mock(return_value=16, side_effect=count.add)
        # side_effect参数它给mock分配了可替换的结果，覆盖了return_value
        # count.add = mock.Mock(return_value=16, side_effect=count.subtract)
        result = count.add(8, 8)
        print(result)
        count.add.assert_called_with(8, 8)
        self.assertEqual(result, 16)


if __name__ == '__main__':
    unittest.main()
