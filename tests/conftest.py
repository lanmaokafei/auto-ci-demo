# -*- coding: utf-8 -*-
"""

@version: ??
@author: xyj
@license: MIT License
@contact: xieyingjun@vip.qq.com
@Created on 2018/4/3 14:02
"""

import sys

import flake8

flake8.configure_logging(2, 'test-logs-%s.%s.log' % sys.version_info[0:2])
