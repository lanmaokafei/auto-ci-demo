# -*- coding: utf-8 -*-

"""
@version: ??
@author: xyj
@license: MIT License
@contact: xieyingjun@vip.qq.com
@Created on 2018/4/3 14:48

简单计算

"""


class Calculator:
    """计算器"""

    @classmethod
    def add(cls, param_a, param_b):
        """
        加法

        :param param_a: 数字a
        :param param_b: 数字b
        :return:  数字a+b
        """
        return param_a + param_b

    @staticmethod
    def subtract(param_a, param_b):
        """
        减法

        :param param_a:  数字a
        :param param_b:  数字b
        :return:  数字a-b
        """
        return param_a - param_b
