# -*- coding: utf-8 -*-

"""
@version: ??
@author: xyj
@license: MIT License
@contact: xieyingjun@vip.qq.com
@Created on 2018/4/3 17:26
"""
import os
from codecs import open
from os import path

from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))

# 加载README文件
readme = 'README.md'
if os.path.exists('README.rst'):
    readme = 'README.rst'
with open(readme, 'rb') as f:
    long_description = f.read().decode('utf-8')

# 读取依赖文件
with open('requirements.txt') as f:
    requirements = [l for l in f.read().splitlines() if l]

setup(
    name='xyj',  # 包名
    version='0.0.1',  # 版本号
    description='YCT SDK for Python',  # 描述
    long_description=long_description,  # 详情
    url='https://gitlab.com/lanmaokafei/auto-ci-demo',  # 项目URL
    author='gft_xyj',  # 作者
    author_email='gft_xyj@qq.com',  # 作者邮箱
    license='MIT',  # 授权
    classifiers=[  # 分类器
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS',
        'Operating System :: POSIX',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.6',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    keywords='XYJ SDK, CI',  # 关键字
    packages=find_packages(),  # 手动指定软件包目录
    install_requires=requirements,  # 依赖包
)
