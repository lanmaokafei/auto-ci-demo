.. auto-ci-demo documentation master file, created by
   sphinx-quickstart on Tue Apr 10 16:57:37 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to auto-ci-demo's documentation!
========================================

auto-ci-demo 是一个简易自动化持续集成的demo，实现了提交代码自动检查代码规范，检查版本兼容，代码安全审计。
所有测试通过后自动生成文档。

计算器API
==================

.. toctree::
   :maxdepth: 2

   calcuator



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
